package clogger

import (
	"fmt"
	"log"

	"github.com/slack-go/slack"
)

var slackClient *slack.Client

// SlackGroups map for each logtype `map[LOG TYPE][]GroupID`
type SlackGroups map[int][]string

var slackGroups SlackGroups

// SetSlack inits the slack client with an api key and groups
func SetSlack(key string, groups SlackGroups) {
	slackClient = slack.New(key)
	slackGroups = groups
}

func slackWrite(logType int, msg string) {
	if slackGroups == nil {
		return //Slack not init
	}

	for _, slackGroup := range slackGroups[logType] {
		_, _, err := slackClient.PostMessage(slackGroup, slack.MsgOptionText(msg, true))
		if err != nil {
			log.Println(fmt.Sprintf("(error) There was an error sending log to slack: %s", err))
			return
		}
	}
}
