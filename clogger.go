package clogger

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"
)

// development deffinition
var development = map[string]bool{
	"development": true,
	"dev":         true,
	"test":        true,
}

const (
	LOG     = 1
	WARNING = 2
	ERROR   = 3
	FATAL   = 4
)

// Set the private variables for the logger
var environment string
var file *os.File

// Init will initialze the logger for use
func Init(env string, logFilePath string) error {
	// Set the environment
	environment = strings.ToLower(env)

	// Set the log output to the console if in dev mode
	if isDev() {
		log.SetOutput(os.Stdout)
		return nil
	}

	// Make sure the file path is defined if not development
	if logFilePath == "" {
		return errors.New("cant have empty file path if not in dev")
	}

	// Open the log file
	file, err := os.OpenFile(logFilePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}

	// Set log output to the file
	log.SetOutput(file)

	return nil
}

// Close out the log file before exiting
func Close() {
	file.Close()
}

// Log logs a normal log
func Log(msg string, variables ...interface{}) {
	// Write the message to the file
	write(LOG, msg, variables...)
}

// Warn logs a warning
func Warn(msg string, variables ...interface{}) {
	// Get the stack trace
	trace := make([]byte, 1024)
	_ = runtime.Stack(trace, true)

	// Remove any empty bytes
	trace = bytes.Trim(trace, "\x00")

	// Add the variable for the stack trace into the set
	msg = "(warn) " + msg + "\n %v"
	variables = append(variables, string(trace))

	// Write the message to the file
	write(WARNING, msg, variables...)
}

// Error logs an error
func Error(msg string, variables ...interface{}) {
	// Get the stack trace
	trace := make([]byte, 1024)
	_ = runtime.Stack(trace, true)

	// Remove any empty bytes
	trace = bytes.Trim(trace, "\x00")

	// Add the variable for the stack trace into the set
	msg = "(error) " + msg + "\n %v"
	variables = append(variables, string(trace))

	// Write the message to the file
	write(ERROR, msg, variables...)
}

// Fatal logs a fatal error that stopped the processing of the API
func Fatal(msg string, variables ...interface{}) {
	// Get the stack trace
	trace := make([]byte, 1024)
	_ = runtime.Stack(trace, true)

	// Remove any empty bytes
	trace = bytes.Trim(trace, "\x00")

	// Add the variable for the stack trace into the set
	msg = "(fatal) " + msg + "\n %v"
	variables = append(variables, string(trace))

	// Write the message to the file
	write(FATAL, msg, variables...)
}

// Write writes the log to the file and integrations
func write(logType int, msg string, variables ...interface{}) {
	// Generate full message string
	fullmsg := fmt.Sprintf(msg, variables...)

	// Write the log
	log.Println(fullmsg)

	// Send it off to integrations
	if slackGroups != nil {
		go slackWrite(logType, fullmsg)
	}
}

// isDev checks to see if the environment is development
func isDev() bool {
	// Check to see if the environment is in the list of development flags
	if _, ok := development[environment]; ok {
		return true
	}

	return false
}
